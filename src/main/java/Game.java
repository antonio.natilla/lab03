

public class Game
{
    int[][] frames;
    int frame_counter;
    boolean is_first_roll;


    public Game()
    {
        frames = new int[12][2];
        frame_counter = 0;
        is_first_roll = true;
    }

    public void roll(int n)
    {
        if(frame_counter > 9 && frame_counter < 11)
        {
            frames[frame_counter][0] = n;
            frame_counter++;
            return;
        }


        if(is_first_roll)
        {
            frames[frame_counter][0] = n;
            if(n == 10)
                frame_counter++;
            else
                is_first_roll = false;
        }
        else
        {
            frames[frame_counter][1] = n;
            frame_counter++;
            is_first_roll = true;
        }
    }

    public int score()
    {
        int s = 0;

        for(int i = 0; i < 10; i++)
        {
            s += frames[i][0];
            System.out.println(frames[i][0]);
            if(frames[i][0] == 10) //if strike
            {
                if(i < 9)
                {
                    s += frames[i+1][0];

                    if(frames[i+1][0] == 10) //if striked again
                        s += frames[i+2][0];
                    else
                        s += frames[i+1][1];
                }
                else
                {
                    s += frames[i+1][0] + frames[i+2][0];
                }

            }
            else
            {
                s += frames[i][1];
                if(i < 9)
                {
                    if(frames[i][1] + frames[i][0] == 10) //if spare
                    {
                        s += frames[i+1][0];
                    }
                }
                else
                {
                    s += frames[i+1][0];
                }
            }
        }

        frame_counter = 0;
        return s;
    }



}