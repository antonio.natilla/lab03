import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main
{
    public static void main(String[] args)
    {
        Game newGame = new Game();
        Logger logger = LoggerFactory.getLogger(Main.class);

        int score = 10;
        for(int i=0; i < 12; i++)
        {
            newGame.roll(score);
            logger.info("{} was the score of the roll number {}", score, i+1);
        }
    }


}
